import UIKit

class BaseViewController: UIViewController {

    private let networkReachabilityListener = NetworkReachabilityListener()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkReachabilityListener.observe(
            handler: networkConnection(didChangeFrom:to:)
        )
    }

    override func viewDidDisappear(_ animated: Bool) {
        networkReachabilityListener.stopObserving()
        super.viewDidDisappear(animated)
    }

    open func networkConnection(didChangeFrom oldState: NetworkState,
                                to newState: NetworkState) {
        print("On network changed from \(oldState) to \(newState)")
    }

    func showAlert(
        title: String, message: String, actions: [UIAlertAction] = [],
        preferredStyle: UIAlertController.Style = .alert
    ) {
        let localizedTitle = NSLocalizedString(title, comment: "")
        let localizedMessage = NSLocalizedString(message, comment: "")
        let alertController = UIAlertController(
            title: localizedTitle, message: localizedMessage,
            preferredStyle: preferredStyle
        )

        actions.forEach { alertController.addAction($0) }
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        self.dismiss(animated: false, completion: nil)
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
}
