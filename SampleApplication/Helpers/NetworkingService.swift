import Foundation

class NetworkingService {
    typealias ExtendedDecodable = Decodable & ConvertibleToClientResult
    typealias Parameters = [String : String]
    typealias NetworkCompletion = (Data?, URLResponse?, Error?) -> Void
    
    private let session: URLSession
    
    init(session: URLSession = .shared) {
        self.session = session
    }
    
    func post<T: ExtendedDecodable>(
        to endpoint: String,
        encodedWith parameters: Parameters,
        then completion: @escaping (Result<T>) -> Void,
        runOn queue: DispatchQueue = .main,
        allowedResponseCodes: ClosedRange<Int> = 200...299
    ) {
        let noData = NetworkingError(message: "Invalid response: No data found.")
        let badData = NetworkingError(message: "Bad data: Cannot decode response data.")
        let badParam = NetworkingError(message: "Invalid parameter encoding.")
        let noResponse = NetworkingError(message: "Invalid response: Response cannot be nil.")
        let badEndpoint = NetworkingError(message: "Invalid endpoint url.")
        let unexpectedStatusCode: (Int) -> NetworkingError = { code in
            return NetworkingError(message: "Status code \(code) is unexpected.")
        }
        
        guard let url = URL(string: endpoint) else {
            return queue.async {
                completion(.error(badEndpoint))
            }
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpShouldHandleCookies = true

        do {
            try request.encoded(with: parameters)
        } catch {
            return queue.async {
                completion(.error(badParam))
            }
        }

        let networkCompletion: NetworkCompletion = { (data, response, error) in
            queue.async {
                if let er = error {
                    let ne = NetworkingError(message: "Unknown error: \(er.localizedDescription)")
                    return completion(.error(ne))
                }
                
                guard let response = response as? HTTPURLResponse else {
                    return completion(.error(noResponse))
                }
                
                guard allowedResponseCodes.contains(response.statusCode) else {
                    return completion(.error(unexpectedStatusCode(response.statusCode)))
                }
                
                
                guard let data = data else {
                    return completion(.error(noData))
                }
                
                do {
                    let decoder = JSONDecoder()
                    let clientData = try decoder.decode(T.self, from: data)
                    return completion(.data(clientData))
                } catch {
                    return completion(.error(badData))
                }
            }
        }
        
        let task = session.dataTask(with: request, completionHandler: networkCompletion)
        task.resume()
    }

    class NetworkingError: Error {
        let message: String
        init(message: String) {
            self.message = message
        }
        
        lazy var localizedMessage: String = {
            return NSLocalizedString(message, comment: "")
        }()
    }

    enum Result<Data> {
        case data(Data)
        case error(NetworkingError)
    }
}

extension URLRequest {
    mutating func encoded(with params: NetworkingService.Parameters) throws {
        self.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        let httpBody =
            try JSONSerialization.data( withJSONObject: params, options: [])
        self.httpBody = httpBody
    }
}


protocol ConvertibleToClientResult {
    associatedtype ClientType
    func toClientResult() -> NetworkingService.Result<ClientType>
}

extension NetworkingService.Result where Data : ConvertibleToClientResult {
    func unwrap() -> NetworkingService.Result<Data.ClientType> {
        switch self {
        case .data(let data):
            return data.toClientResult()
        case .error(let error):
            return .error(error)
        }
    }
}
