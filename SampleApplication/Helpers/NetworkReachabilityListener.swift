import SystemConfiguration

class NetworkReachabilityListener {
    typealias ReachabilityEventHandler =
        (_ oldState: NetworkState, _ newState: NetworkState) -> Void
    
    private var reachability: SCNetworkReachability =
        SCNetworkReachabilityCreateWithName(nil, "www.google.com")!
    private lazy var networkContext: SCNetworkReachabilityContext =
        SCNetworkReachabilityContext(
            version: 0, info: nil, retain: nil,
            release: nil, copyDescription: nil
    )
    
    
    private var handler: ReachabilityEventHandler?
    private var currentReachabilityFlags: SCNetworkReachabilityFlags?
    private var currentState = NetworkState.unknown
    private var isPending = true
    
    func observe(handler: @escaping ReachabilityEventHandler,
                 on handlerQueue: DispatchQueue = .main) {
        self.handler = handler
        guard isPending else {
            handler(.unknown, currentState)
            return
        }
        
        // What is this??
        networkContext.info = UnsafeMutableRawPointer(
            Unmanaged<NetworkReachabilityListener>.passUnretained(self).toOpaque()
        )
        
        let callback: SCNetworkReachabilityCallBack = {
            (reachability: SCNetworkReachability,
            flags: SCNetworkReachabilityFlags, info: UnsafeMutableRawPointer?) in
            
            guard let info = info else { return }
            let handler = Unmanaged<NetworkReachabilityListener>
                .fromOpaque(info).takeUnretainedValue()
            
            DispatchQueue.main.async {
                handler.checkReachability(flags: flags)
            }
        }
        
        SCNetworkReachabilitySetCallback(reachability, callback, &networkContext)
        SCNetworkReachabilitySetDispatchQueue(reachability, handlerQueue)
        handlerQueue.async {
            self.currentReachabilityFlags = nil
            var flags = SCNetworkReachabilityFlags()
            SCNetworkReachabilityGetFlags(self.reachability, &flags)
            self.checkReachability(flags: flags)
        }
        
        isPending = false
    }
    
    private func checkReachability(flags: SCNetworkReachabilityFlags) {
        currentReachabilityFlags = flags
        let oldState = currentState
        let newState: NetworkState
        
        if isNetworkReachable(with: flags) {
            if flags.contains(.isWWAN) {
                newState = .cellular
            } else {
                newState = .wifi
            }
        } else {
            newState = .disconnected
        }
        
        guard oldState != newState else {
            return
        }
        
        currentState = newState
        handler?(oldState, currentState)
    }
    
    func stopObserving() {
        if isPending {
            return
        }
        
        SCNetworkReachabilitySetCallback(reachability, nil, nil)
        SCNetworkReachabilitySetDispatchQueue(reachability, nil)
        isPending = true
    }
    
    private func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
}

enum NetworkState {
    case cellular
    case wifi
    case disconnected
    case unknown
    
    var reachable: Bool {
        return self == .wifi || self == .cellular
    }
}
