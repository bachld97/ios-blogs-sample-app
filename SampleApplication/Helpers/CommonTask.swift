protocol CommonTask {
    func commonTaskShowLoading()
    func commonTaskShowNoInternet()
}

extension CommonTask {
    func commonTaskShowLoading() { }
    func commonTaskShowNoInternet() { }
}
