struct LoginResponse: Decodable, ConvertibleToClientResult {
    typealias ClientType = User
    
    private let token: String
    private let status: Int
    private let message: String
    private let user: UData?


    struct UData: Decodable {
        let username: String
        let avatar: String
        let dob: String
        
        func toUserInfo(token: String) -> User {
            return User(name: username, avatar: avatar,
                        dob: dob, token: token)
        }
    }
    
    func toClientResult() -> NetworkingService.Result<User> {
        guard let u = user else {
            return .error(LoginError(message: message))
        }
        
        return .data(u.toUserInfo(token: token))
    }
}
