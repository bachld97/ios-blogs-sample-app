protocol LoginUseCaseDelegate: class {
    func loginUseCase(didFail error: LoginError)
    func loginUseCase(didSucceed user: User)
}

protocol LoginScreenActionHandler: class, CommonTask {
    func loginScreenActionHanlder(performLogin username: String, password: String)
    func loginScreenActionHandlerNavigateToSignUp()
}

