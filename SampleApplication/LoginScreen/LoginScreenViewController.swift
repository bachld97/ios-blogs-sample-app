import UIKit

class LoginScreenViewController: BaseViewController {
    private lazy var loginScreenView = LoginScreenView(handler: self)
    private lazy var loginUseCase = LoginUseCase(delegate: self)

    override func loadView() {
        view = loginScreenView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }
    
    override func networkConnection(didChangeFrom oldState: NetworkState, to newState: NetworkState) {
        super.networkConnection(didChangeFrom: oldState, to: newState)
        loginScreenView.isNetworkReachable = newState.reachable
    }
    
    private func goToHome(as user: User) {
        let homeVc = HomeScreenViewController(user)
        self.present(homeVc, animated: true, completion: nil)
    }
    
    private func goToSignUp() {
        let signupVc = SignupScreenViewController()
        self.present(signupVc, animated: true, completion: nil)
    }
}

extension LoginScreenViewController: LoginScreenActionHandler {
    func loginScreenActionHanlder(performLogin username: String, password: String) {
        loginUseCase.execute(username: username, password: password)
    }
    
    func loginScreenActionHandlerNavigateToSignUp() {
        self.goToSignUp()
    }
    
    func commonTaskShowLoading() {
        
    }
    
    func commonTaskShowNoInternet() {
        let actions = [
            UIAlertAction(title: "I understand", style: .cancel, handler: nil)
        ]
        
        self.showAlert(
            title: "Could not login",
            message: "Please check your internet connection and try again later.",
            actions: actions
        )
    }
}

extension LoginScreenViewController: LoginUseCaseDelegate {
    func loginUseCase(didFail error: LoginError) {
        loginScreenView.errorMessage = error.localizedMessage
    }
    
    func loginUseCase(didSucceed user: User) {
        self.goToHome(as: user)
    }
}
