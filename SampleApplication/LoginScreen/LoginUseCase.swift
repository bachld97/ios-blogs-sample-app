import Foundation
class LoginUseCase {
    typealias LoginResult = NetworkingService.Result<LoginResponse>
    private weak var delegate: LoginUseCaseDelegate?
    private let networkingService: NetworkingService
    
    init(delegate: LoginUseCaseDelegate,
         networkingService: NetworkingService = .init()) {
        self.delegate = delegate
        self.networkingService = networkingService
    }
    
    func execute(username: String, password: String) {
        guard !username.isEmpty, !password.isEmpty else {
            let er = LoginError(message: "Username or password is empty.")
            delegate?.loginUseCase(didFail: er)
            return
        }
        
        let url = Endpoint.login
        let params = [
            "username": username,
            "password": password
        ]
        
        networkingService.post(to: url, encodedWith: params, then: completion)
    }
    
    lazy var completion: (LoginResult) -> Void = { [weak self] loginResult in
        let result = loginResult.unwrap()
        switch result {
        case .data(let user):
            self?.delegate?.loginUseCase(didSucceed: user)
        case .error(let error):
            // Meaningful error message to user
            if let e = error as? LoginError {
                self?.delegate?.loginUseCase(didFail: e)
                return
            }
            
            // Meaningful error message to developer
            // logger.log(error.message)
            let e = LoginError(message: "Unexpected error - Try again later.")
            self?.delegate?.loginUseCase(didFail: e)
        }
    }
}

class LoginError: NetworkingService.NetworkingError { }
