class Endpoint {
    private init() {} // Static access only
    
    static let login = "http://127.0.0.1:8000/accounts/login/"
    static let signup = "http://127.0.0.1:8000/accounts/signup/"
}
