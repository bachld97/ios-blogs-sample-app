protocol SignupScreenActionHandler: class, CommonTask {
    func signupScreenActionHandler(
        signupWith username: String, email: String, password: String)
    
    func signupScreenActionHandlerNavigateToLogin()
}

protocol SignupUseCaseDelegate: class {
    func signupUseCase(didFail error: SignupError)
    func signupUseCase(didSucceed createdUser: User)
}
