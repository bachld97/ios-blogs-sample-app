import UIKit

class SignupScreenView: UIView {
    private weak var handler: SignupScreenActionHandler?

    init(handler: SignupScreenActionHandler) {
        self.handler = handler
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
