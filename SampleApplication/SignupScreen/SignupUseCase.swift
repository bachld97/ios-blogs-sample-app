class SignupUseCase {
    typealias SignupResult = NetworkingService.Result<SignupResponse>

    private weak var delegate: SignupUseCaseDelegate?
    private let networkingService: NetworkingService

    init(delegate: SignupUseCaseDelegate,
         networkingService: NetworkingService = .init()) {
        self.delegate = delegate
        self.networkingService = networkingService
    }
    
    
    func execute(username: String, password: String, email: String) {
        guard !username.isEmpty, !password.isEmpty, !email.isEmpty else {
            let er = SignupError(message: "Username, password, email cannot be empty.")
            delegate?.signupUseCase(didFail: er)
            return
        }
        let url = Endpoint.signup
        let params = [
            "username": username,
            "password": password,
            "email": email
        ]
        
        networkingService.post(to: url, encodedWith: params, then: completion)
    }
    
    lazy var completion: (SignupResult) -> Void = { [weak self] signupResult in
        let result = signupResult.unwrap()
        switch result {
        case .data(let user):
            self?.delegate?.signupUseCase(didSucceed: user)
        case .error(let error):
            // Meaningful error message to user
            if let e = error as? SignupError {
                self?.delegate?.signupUseCase(didFail: e)
                return
            }
            
            // Meaningful error message to developer
            // logger.log(error.message)
            let e = SignupError(message: "Unexpected error - Try again later.")
            self?.delegate?.signupUseCase(didFail: e)
        }
    }
}

class SignupError: NetworkingService.NetworkingError { }
