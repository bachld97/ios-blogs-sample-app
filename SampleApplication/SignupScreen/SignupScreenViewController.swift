import UIKit

class SignupScreenViewController: BaseViewController {
    private lazy var signupScreenView = SignupScreenView(handler: self)
    private lazy var signupUseCase = SignupUseCase(delegate: self)

    override func loadView() {
        view = signupScreenView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }
    
    override func networkConnection(didChangeFrom oldState: NetworkState, to newState: NetworkState) {
        super.networkConnection(didChangeFrom: oldState, to: newState)
    }
    
    private func goToHome(as user: User) {
        let homeVc = HomeScreenViewController(user)
        self.present(homeVc, animated: true, completion: nil)
    }
    
    private func goToLogin() {
        let loginVc = LoginScreenViewController()
        self.present(loginVc, animated: true, completion: nil)
    }
}

extension SignupScreenViewController: SignupScreenActionHandler {
    func signupScreenActionHandler(signupWith username: String, email: String, password: String) {
        signupUseCase.execute(
            username: username, password: password, email: email
        )
    }
    
    func signupScreenActionHandlerNavigateToLogin() {
        goToLogin()
    }
    
    func commonTaskShowNoInternet() {
        let actions = [
            UIAlertAction(title: "I understand", style: .cancel, handler: nil)
        ]
        
        self.showAlert(
            title: "Could not signup",
            message: "Please check your internet connection and try again later.",
            actions: actions
        )
    }
}

extension SignupScreenViewController: SignupUseCaseDelegate {
    func signupUseCase(didFail error: SignupError) {
        
    }
    
    func signupUseCase(didSucceed createdUser: User) {
        
    }
}
