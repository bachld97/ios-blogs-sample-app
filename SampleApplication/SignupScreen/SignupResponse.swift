struct SignupResponse: Decodable, ConvertibleToClientResult {
    typealias ClientType = User
    
    func toClientResult() -> NetworkingService.Result<User> {
        fatalError()
    }
}
